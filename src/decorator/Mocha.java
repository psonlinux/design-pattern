package decorator;

public class Mocha extends CondimentDecorator{
	private Beverage b;
	public Mocha(Beverage b){
		this.b = b;
	}
	
	public String getDescription() {
		return b.getDescription() + " Mocha ";
	}
	
	public int cost() {
		return 2 + b.cost();
	}
}

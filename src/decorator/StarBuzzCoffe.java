package decorator;

public class StarBuzzCoffe {
	public static void main(String[] args) {
		Beverage es = new Espresso();
		System.out.println(es.getDescription() + " " + es.cost());
		
		CondimentDecorator mocha = new Mocha(es);
		mocha = new Mocha(mocha);
		System.out.println(mocha.getDescription() + " " + mocha.cost());
	}
}

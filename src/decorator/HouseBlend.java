package decorator;

public class HouseBlend extends Beverage{
	public HouseBlend() {
		description = "HouseBlend";
	}
	
	public int cost() {
		return 10;
	}
}

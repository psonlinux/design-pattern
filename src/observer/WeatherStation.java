package observer;

public class WeatherStation {

	public static void main(String[] args) {
		WeatherData wd = new WeatherData();
		CurrentConditionDisplay ccd = new CurrentConditionDisplay(wd);
		StatisticsDisplay sd = new StatisticsDisplay(wd);
		
		wd.setMeasurement(30.0, 82.5, 101.02);
		wd.setMeasurement(66.0, 90.0, 112.90);
	}

}

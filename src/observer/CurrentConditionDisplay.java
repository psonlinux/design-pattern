package observer;

public class CurrentConditionDisplay implements Observer, DisplayElement{
	private double temperature, humidity;
	public CurrentConditionDisplay(Subject subject) {
		subject.registerObserver(this);
	}

	@Override
	public void display() {
		System.out.println("Temperature = " + temperature + " Humidity = " + humidity);
	}

	@Override
	public void update(double temperature, double humidity, double pressure) {
		this.temperature = temperature;
		this.humidity = humidity;
		display();
	}
}
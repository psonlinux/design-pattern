package observer;

import java.util.ArrayList;
import java.util.Collections;

public class StatisticsDisplay implements Observer, DisplayElement{
	private ArrayList<Double> tempList = new ArrayList<>();
	
	public StatisticsDisplay(Subject subject) {
		subject.registerObserver(this);
	}
	
	@Override
	public void display() {
		System.out.println("Min temp = " + tempList.get(0) + " Max temp = " + tempList.get(tempList.size() - 1));
	}

	@Override
	public void update(double temperature, double humidity, double pressure) {
		tempList.add(temperature);
		Collections.sort(tempList);
		display();
	}
	
}

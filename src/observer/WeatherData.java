package observer;

import java.util.ArrayList;

public class WeatherData implements Subject{
	private double temp, humidity, pressure;
	ArrayList<Observer> observers = new ArrayList<>();

	@Override
	public void registerObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public void removeObserver(Observer o) {
		int index = observers.indexOf(o);
		if(index >= 0) {
			observers.remove(index);
		}
	}

	@Override
	public void notifyObservers() {
		for(int i = 0; i < observers.size(); i++) {
			observers.get(i).update(temp, humidity, pressure);
		}
	}
	
	public void setMeasurement(double temp, double humidity, double p) {
		this.humidity 	= humidity;
		this.temp 		= temp;
		this.pressure 	= p;
		measurementChanged();
	}
	
	public void measurementChanged() {
		notifyObservers();
	}
}

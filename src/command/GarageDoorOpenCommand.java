package command;

public class GarageDoorOpenCommand implements Command{
	private GarageDoor gd;
	public GarageDoorOpenCommand(GarageDoor gd) {
		this.gd = gd;
	}
	

	@Override
	public void execute() {
		// TODO Auto-generated method stub
		gd.up();
	}


	@Override
	public void undo() {
		gd.down();
	}
	
	
}

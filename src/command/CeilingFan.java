package command;

public class CeilingFan {
	static final int HIGH = 3;
	static final int MEDIUM = 2;
	static final int LOW = 1;
	static final int OFF = 0;
	private int speed;
	private String location;
	
	public CeilingFan(String location) {
		this.location = location;
		speed = OFF;
	}
	
	public void high() {
		speed = HIGH;
	}
	
	public void medium() {
		speed = MEDIUM;
	}
	
	public void low() {
		speed = LOW;
	}
	
	public void off() {
		speed = OFF;
	}
	
	public int getSpeed() {
		return speed;
	}
}

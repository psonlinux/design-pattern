package command;

public class RemoteLoader {

	public static void main(String[] args) {
		
		
		Light livingRoomLight = new Light("Living room");
		LightOnCommand loc = new LightOnCommand(livingRoomLight);
		LightOffCommand lightOffCom = new LightOffCommand(livingRoomLight);
		
		CeilingFan cf = new CeilingFan("Bedroom ");
		CeilingFanHighCommand cfhc = new CeilingFanHighCommand(cf);
		CeilingFanOffCommand cfoc = new CeilingFanOffCommand(cf);
		
		int devices = 4;
		RemoteControl rc = new RemoteControl(devices);
		rc.setCommand(0, loc, lightOffCom);
		rc.setCommand(1, cfhc, cfoc);
		
		System.out.print(rc);
		
		rc.onButtonWasPressed(0);
		rc.offButtonWasPressed(0);
		
		//undo light off operation
		rc.undoButtonWasPressed();
		
		rc.onButtonWasPressed(1);
		rc.offButtonWasPressed(1);
		rc.undoButtonWasPressed();
		System.out.println(rc);
	}

}

package command;

public class GarageDoor {
	public void up() {
		System.out.println("Garage door open.");
	}
	
	public void down() {
		
	}
	
	public void stop() {
		
	}
	
	public void lightOn() {
		
	}
	
	public void lightOff() {
		
	}
}

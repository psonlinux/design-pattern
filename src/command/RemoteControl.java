package command;

public class RemoteControl {
	private Command[] onCommands;
	private Command[] offCommands;
	private Command undoCommand;
	public RemoteControl(int devices) {
		onCommands = new Command[devices];
		offCommands = new Command[devices];
		
		NoCommand noc = new NoCommand();
		for(int i = 0; i < devices; i++) {
			onCommands[i] = noc;
			offCommands[i] = noc;
		}
		undoCommand = noc;
	}
	
	public void setCommand(int slot, Command onCommand, Command offCommand) {
		onCommands[slot] = onCommand;
		offCommands[slot] = offCommand;
	}
	
	public void onButtonWasPressed(int slot) {
		onCommands[slot].execute();
		undoCommand = onCommands[slot];
	}
	
	public void offButtonWasPressed(int slot) {
		offCommands[slot].execute();
		undoCommand = offCommands[slot];
	}
	
	public void undoButtonWasPressed() {
		undoCommand.undo();
	}
	
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("\n---------------Remote-------------\n");
		for(int i = 0; i < onCommands.length; i++) {
			sb.append("slot[" + i + "] " + onCommands[i].getClass().getName() + " " + offCommands[i].getClass().getName() + "\n");
		}
		sb.append(undoCommand.getClass().getName() + " " + undoCommand.getClass().getName() + "\n");
		return sb.toString();
	}
}

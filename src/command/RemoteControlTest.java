package command;

public class RemoteControlTest {

	public static void main(String[] args) {
		GarageDoor gd = new GarageDoor();
		GarageDoorOpenCommand gdc = new GarageDoorOpenCommand(gd);
		
		Light light = new Light("living room");
		LightOnCommand loc = new LightOnCommand(light);
		
		SimpleRemoteControl src = new SimpleRemoteControl();
		src.setCommand(gdc);
		src.buttonWasPressed();
		
		src.setCommand(loc);
		src.buttonWasPressed();
	}

}

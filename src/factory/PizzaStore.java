package factory;

public abstract class PizzaStore {
	String name;
	Pizza orderPizza(String type) {
		Pizza pizza = createPizza(type);
		return pizza;
	}
	
	public abstract Pizza createPizza(String type);
}

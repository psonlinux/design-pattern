package factory;

public class NYPizzaStore extends PizzaStore{
	private PizzaIngredientFactory pif;
	public NYPizzaStore(PizzaIngredientFactory pif) {
		this.pif = pif;
	}
	
	@Override
	public Pizza createPizza(String type) {
		if(type.equals("cheese")) {
			return new NYStyleCheesePizza(pif);
		}
		else if(type.equals("pepporoni")) {
			return new NYStylePepporoniPizza(pif);
		}
		else {
			return null;
		}
	}
	
	
}

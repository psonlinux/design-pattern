package factory;

public abstract class Cheese {
	String type;
	Integer cost;
	public abstract void setType();
	public abstract void setCost();
	public abstract Integer getCost();
}

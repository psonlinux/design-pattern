package factory;

public class NYStyleCheesePizza extends Pizza{
	private PizzaIngredientFactory pif;
	public NYStyleCheesePizza(PizzaIngredientFactory pif) {
		this.pif = pif;
	}
	
	public void prepare() {
		dough = pif.createDough();
		sauce = pif.createSauce();
		cheese = pif.createCheese();
	}
}

package factory;

public class NYStylePepporoniPizza extends Pizza {
	private PizzaIngredientFactory pif;
	public NYStylePepporoniPizza(PizzaIngredientFactory pif) {
		this.pif = pif;
	}
	
	@Override
	public void prepare() {
		cheese = pif.createCheese();
		dough = pif.createDough();
		sauce = pif.createSauce();
	}
}

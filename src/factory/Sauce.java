package factory;

public abstract class Sauce {
	String type;
	Integer cost;
	public abstract void setType();
	public abstract void setCost();
	public abstract Integer getCost();
}
